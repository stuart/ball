ball (1.5.0+git20180813.37fc53c-4) unstable; urgency=medium

  * Team upload (Closes: #948438)
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Remove trailing whitespace in debian/rules (routine-update)
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.
  * Add Mult-Arch hints
  * build-dep on python2-dev, not the soon to be removed python-dev

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Wed, 19 Feb 2020 15:20:40 +0100

ball (1.5.0+git20180813.37fc53c-3) unstable; urgency=medium

  * Add missing Breaks+Replaces: libballview1.4-dev
    Closes: #919225
  * Enable building with dpkg-buildpackage -A
    Closes: #919239

 -- Andreas Tille <tille@debian.org>  Mon, 14 Jan 2019 08:12:12 +0100

ball (1.5.0+git20180813.37fc53c-2) unstable; urgency=medium

  * Fix Build-Depends (Thanks for the hints to Adrian Bunk
    <bunk@debian.org>)
    Closes: #784451
  * Fix FTBFS on architectures where char is unsigned
    (Thanks for the fix to Adrian Bunk <bunk@debian.org>)
    Closes: #919172

 -- Andreas Tille <tille@debian.org>  Sun, 13 Jan 2019 15:49:01 +0100

ball (1.5.0+git20180813.37fc53c-1) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.
  * Trim trailing whitespace.

  [ Andreas Tille ]
  * New upstream version
    Closes: #784451, #905443, #911881
  * Use Git mode in watch file
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.3.0
  * Switch to Qt5
    Closes: #874837
  * Drop python-ballview package
  * Do not fail in case of build time test failures
  * d/control: Remove X-Python-Version field
  * d/rules: Remove manual CFLAGS settings
  * d/rules: Remove cruft from python-ball package
  * Use wrapper to export BALL_DATA_PATH

 -- Andreas Tille <tille@debian.org>  Thu, 10 Jan 2019 20:52:01 +0100

ball (1.4.3~beta1-4) unstable; urgency=medium

  * FTBFS with sip 4.19.x (Thanks for the hint to upsteam solution given
    by Dmitry Shachnev <mitya57@debian.org>)
    Closes: #867660
  * debhelper 10
  * Standards-Version: 4.1.0 (no changes needed)
  * Add dh_sip after dh_install (Thanks for the hint to Dmitry Shachnev
    <mitya57@debian.org>)
  * Do not run test suite parallel
  * Drop test that leads frequently to unpredictable failures
  * Desperately remove all tests that tend to fail randomly

 -- Andreas Tille <tille@debian.org>  Thu, 21 Sep 2017 14:44:17 +0200

ball (1.4.3~beta1-3) unstable; urgency=medium

  * Ignore test suite result on non-amd64
    Closes: #830984

 -- Danny Edel <debian@danny-edel.de>  Sun, 13 Nov 2016 18:24:33 +0100

ball (1.4.3~beta1-2) unstable; urgency=medium

  [ Danny Edel ]
  * Team upload
  * Fix FTBFS with GCC-6 and recent boost
    Closes: #830984, #833004
    * Patches backported from upstream git:
      * 2822068 Modify RTTI function isKindOf to take a pointer argument
        (this fixes a segfault in DefaultProcessors_test)
      * 0604808 Fix PoseClustering_test: replace serialized file comparison
      * 7380918 Fix XDRPersistenceManager_test
    * New patch: disable AmberFF_test, which fails nondeterministically
  * Drop previous PoseClustering_test patches (no longer needed)
  * d/watch: fix uversionmangle expression to correctly detect -BETA

  [ Andreas Tille ]
  * Remove useless Breaks in python-ballview and python-ball
  * lintian override for package-name-doesnt-match-sonames

 -- Danny Edel <debian@danny-edel.de>  Fri, 11 Nov 2016 12:07:51 +0100

ball (1.4.3~beta1-1) unstable; urgency=medium

  [ Andreas Tille ]
  * New upstream version
    Closes: #777791
  * New homepage
  * Moved to Github
  * Adapt watch file to Github
  * Cherry-pick from upstream "Use c++11 on Clang"
  * Andreas Hildebrandt does not work on the packaging any more
  * cme fix dpkg-control
  * debian/rules: use dh
    Closes: #787888
  * debian/control: Drop *-dbg package since this is created automatically now
  * delete debian/menu
  * polish d/copyright
  * better hardening
  * cme fix dpkg-control
  * do not install empty directories for docs
  * do not repeat section field in d/control
  * doc package can be installed without main package -> only suggests main
    package
  * Use libjs-jquery instead of local copy pf jquery.js
  * DEP5 fixes
  * Use python-helper properly
  * Enhance long description of dev package
  * Use Breaks where Conflicts is to strong
  * Remove extra license file
  * Add lintian override for false positive package-contains-empty-directory

  [ Danny Edel ]
  * d/rules: Enable building and execution of unittests
  * Drop patch gcc5.diff (fixed upstream)
  * Backport patches from upstream git to fix FTBFS on recent sid:
    * b3679aa Fix: String failed to compile with newer GCC
    * 029fd2b Fix building PoseClustering with recent Boost
    * 3170b28 Fix compilation of BinaryFingerprintMethods_test
    * 7bb0166 Fixed PoseClustering test to account for changes in boost
    * 8ff1d1b FingerPrintSim: Fix build under clang 3.6 (also needed on gcc)
    * 38a955a Fix Python bindings for Box
    * f646875 Increased sleeping time on Socket_test
  * Add patches:
    * disable-assign-positions-from-template.patch
      (binary was only built, but not shipped previously, and fails to build
      with libeigen3-dev 3.3~beta1)
    * adjust-poseClustering_Test-boost-1.58.patch

 -- Andreas Tille <tille@debian.org>  Wed, 18 May 2016 14:49:09 +0200

ball (1.4.2+20140406-1.1) unstable; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Non-maintainer upload.
  * Remove qt4-related non-development build dependencies (Closes: #787746).
  * Force linking against libX11 with link_against_x11.patch. It seems that
    now we require to manually explicit it.
  * Add findsip.patch by Dmitry Shachnev to fix sip library detection.

  [ Matthias Klose ]
  * Fix some build errors with GCC 5 and clang++. Closes: #755225.
    Addresses #777791.
  * Build using dh-python. Closes: #785941.

 -- Matthias Klose <doko@debian.org>  Thu, 09 Jul 2015 12:17:06 +0200

ball (1.4.2+20140406-1) unstable; urgency=medium

  [ Steffen Moeller ]
  * New upstream version (upstream reported better compatibility
    with latest compilers)

  [ Andreas Tille ]
  * New upstream version (adapted patches)
  * debian/watch: Fix to report latest upstream version
  * debian/control:
     - cme fix dpkg-control
     - canonical Vcs fields
     - debhelper 9
     - Fix Build-Depends-Indep to enable doxygen documentation
     - Fix Build-Depends (add libglew-dev, drop long outdated versions
       of libboost)
     - Add Build-Depends libqt4-dev-bin for uic-qt4 to auto generate UI
       header files
  * debian/copyright: DEP5
  * debian/rules:
     - do not delete *.doc files since these are also in upstream tarball
     - create UI header files
  * debian/patches/0101-cmake-syntax.patch: Fix cmake
    syntax to prevent annoying warnings in build log
  * debian/patches/0102-fix-boost-compatibility-issue.patch:  Fix build
    issue with more recent boost versions (Thanks to Gert Wollny
    <gw.fossdev@gmail.com> for the patch)
    Closes: #720681
  * debian/upstream: Drop outdated watch information
  * debian/patches/0103-QT4_EXTRACT_OPTIONS-CMake-macro-changed-in-CMake-2.8.patch:
    commit 1e76c9 in the ball git repro
    (thanks to Gert Wollny <gw.fossdev@gmail.com> for the hint)

 -- Steffen Moeller <moeller@debian.org>  Mon, 07 Apr 2014 15:03:11 +0200

ball (1.4.1+20111206-4) unstable; urgency=low

  * Fix compilation with g++ >= 4.7 (Closes: 674226)
  * Updated policy to 3.9.3 (no changes required)

 -- Andreas Hildebrandt <andreas.hildebrandt@uni-mainz.de>  Wed, 15 Aug 2012 00:50:32 +0200

ball (1.4.1+20111206-3) unstable; urgency=low

  * Fixed "const" in .sip files (Closes: #653626)

 -- Steffen Moeller <moeller@debian.org>  Fri, 06 Jan 2012 16:43:51 +0100

ball (1.4.1+20111206-2) unstable; urgency=low

  * Added "signed" to char because of platform differences

 -- Steffen Moeller <moeller@debian.org>  Fri, 09 Dec 2011 13:55:01 +0100

ball (1.4.1+20111206-1) unstable; urgency=low

  * New upstream version.
  * Depending on ghostscript, not gs-common (Closes: #649711)

 -- Andreas Hildebrandt <andreas.hildebrandt@uni-mainz.de>  Thu, 08 Dec 2011 19:53:09 +0100

ball (1.4.0-5) unstable; urgency=low

  * Added myself to Uploaders
  * Rebuilt with current version of python (Closes: #632488).
  * Added dependency on texlive-latex-recommended and the build
    instructions should already separate the documentation from
    the rest (Closes: #631999).
  * Unsigned char issue was at some point already fixed (Closes: #631998).
  * Documentation packages are in conflict (Closes: #631911)
  * Removed (redundant) conflict of ballview to itself
  * Helped quirk in descrition (Closes: #598081)

 -- Steffen Moeller <moeller@debian.org>  Fri, 22 Jul 2011 17:17:06 +0200

ball (1.4.0-3) unstable; urgency=low

  * Added -fpermissive to compiler flags to avoid "narrowing" error

 -- Steffen Moeller <moeller@debian.org>  Wed, 20 Jul 2011 17:47:51 +0200

ball (1.4.0-2) unstable; urgency=low

  * Addressing build failures (Closes: #615670, #631999)
    - removed -j2 in build instructions (Closes: #632483)
    - added texlive-recommends not covered by doxygen-latex

 -- Steffen Moeller <moeller@debian.org>  Wed, 20 Jul 2011 15:19:18 +0200

ball (1.4.0-1) unstable; urgency=low

  [ Andreas Hildebrandt ]
  * Updated to new upstream release 1.4.0
  * Cherry-pick upstream patch for linguist files
  * Cherry-pick upstream patch for new DSO linking scheme
  * Updated policy to 3.9.2
  * Split out arch-independent data package
  * Depend on libgl1-mesa-glx instead of libgl1-mesa-swx11
  * Introduce suitable conflicts against older versions where necessary
  * Install cmake BALL exports and config
  * build-depend on doxygen-latex instead of texlive-* (Closes: 616200)
  * Split build dependencies into Build-Deps and Build-Deps-Indep
  * Depend on python-sip-dev instead of python-sip4-dev (Closes: #611072)

  [ Steffen Möller ]
  * Moved all build instructions to debian/rules.
  * Corrected email address
  * Updated policy to 3.8.4 (no changes required)
  * Added libgl1-mesa-swx11 explicitly as a dependency to ballview

 -- Andreas Hildebrandt <ahildebr@uni-mainz.de>  Tue, 21 Jun 2011 18:29:51 +0200

ball (1.3.2-2) unstable; urgency=low

  [Andreas Hildebrandt]
  * Improvements for CMake build system (Closes: 546311)
  * Some fixes for less common architectures and fixes in the sip dependencies
   (Closes: 565694)
  [Jonathan Wiltshire]
  * Depend on python-support and call dh_pysupport in debian/rules.
    Let python-support decide dependencies for python-ball and python-ballview.
    (Closes: #566036)

 -- Andreas Hildebrandt <anhi@bioinf.uni-sb.de>  Thu, 11 Feb 2010 22:15:09 +0100

ball (1.3.2-1) unstable; urgency=low

  * Updated to new upstream release 1.3.2

 -- Andreas Hildebrandt <anhi@bioinf.uni-sb.de>  Tue, 19 Jan 2010 12:34:33 +0100

ball (1.3.1-2) unstable; urgency=low

  * Add python-sip4 as an explicit build-dependency (fixes an FTBFS)

 -- Andreas Hildebrandt <anhi@bioinf.uni-sb.de>  Tue, 12 Jan 2010 10:25:36 +0100

ball (1.3.1-1) unstable; urgency=low

  [ Andreas Hildebrandt ]
  * Updated to new upstream release 1.3.1 (Closes: 551241)
  * Switched from autoconf to CMake (Closes: 552809)
  * Increased dependency to python-sip4 to version 4.8.2, which
    would need to be adjusted for lenny (4.7.2). The real (unexpressable)
    dependency is to have the same version at build- and run-time.  [Steffen]

  [ Charles Plessy ]
  * Documented informations in ‘debian/upstream-metadata.yaml’.

 -- Andreas Hildebrandt <anhi@bioinf.uni-sb.de>  Fri, 18 Dec 2009 14:20:08 +0100

ball (1.3.0-1) unstable; urgency=low

  * New upstream release
  * BALL no longer uses gSoap (Closes: #542215)
  * Added s390 to list of supported processors (Closes: #546311)
  * Fixed broken maintainer address (Closes: #544932)
  * Updated policy-compliance to 3.8.3 (no changes reguired)
  * Updated build-dependencies to demand more recent libtool

 -- Andreas Hildebrandt <anhi@bioinf.uni-sb.de>  Thu, 17 Sep 2009 22:30:28 +0200

ball (1.3+rc2-1) unstable; urgency=low

  * Removed debian/copyright entries for files no longer in the repository
  * Updated to new upstream release candidate
  * Removed build-dependency on libboost-asio-dev (Closes: #542251)
  * Some small packaging improvements

 -- Andreas Hildebrandt <anhi@bioinf.uni-sb.de>  Thu, 17 Sep 2009 22:25:51 +0200

ball (1.3+beta2.1-2) unstable; urgency=low

  * Initial packaging
  * Re-upload upon initial rejection to improve debian/copyright (Closes: #407665)
  * Improved Doc-Base
  * Eliminated two lintian warnings
  * Preparing for new upstream version 1.3 (Closes: #407665)
  * Improvements for Debian packaging
    - providing VIEW-independent and -dependent debs
    - corrected for problematic copyright statements

 -- Andreas Hildebrandt <anhi@bioinf.uni-sb.de>  Sun, 16 Aug 2009 01:23:30 +0200
